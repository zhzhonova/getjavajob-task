    
//Получить цифровой корень числа;

import java.util.Scanner;

public class TaskCh10N44 { 
    public static long root(long n) {
      
        if (n < 1) {
            return 0;
        }
          return root((root(n / 10) + (n % 10))/10) + (root(n / 10) + (n % 10)) % 10;
        
    }

    
    public static void main(String[] args) {
      
        Scanner input = new Scanner(System.in);
        System.out.println("Введите натуральное число n");
        long n = input.nextLong();
        
        System.out.println("Цифровой корень числа n: " + root(n)); 

    }
}

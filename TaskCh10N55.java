import java.util.Scanner;    
/*
Написать рекурсивную процедуру перевода натурального числа из десятичной системы счисления в N-ричную. 
Значение N в основной программе вводится с клавиатуры (2 N 16).
*/

public class TaskCh10N55 { 
  
    public static void recursion(int k, int s) {
    String[] l = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
    
      if (s < k) 
        System.out.println(l[s]);
 
      else {
          recursion(k, s / k);
          System.out.println(l[s % k]);
      }
  }
  
public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите N систему счисления, в которую Вы хотите перевести ваше число (от 2 до 16 включительно).");
        int N = input.nextInt();
        System.out.println("Введите n натуральное число, которое Вы хотите перевести в другую систему счисления");
        int n = input.nextInt();
        recursion(N, n); 
    }
}

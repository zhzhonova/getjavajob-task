    
//а) вычисления суммы цифр натурального числа;
//б) вычисления количества цифр натурального числа.
import java.util.Scanner;

public class TaskCh10N43 { 
    public static int sum(int n) {
      
        if (n < 1) {
            return 0;
        }
        return sum (n / 10) + (n % 10);
    }
    
    public static int amount(int n) {
      
        if (n < 1) {
            return 0;
        }
        return amount (n / 10) + 1;
    }
    public static void main(String[] args) {
      
        Scanner input = new Scanner(System.in);
        System.out.println("Введите натуральное число n");
        int n = input.nextInt();
        
        System.out.println("Сумма цифр: " + sum(n)); 
        System.out.println("Количество цифр: " + amount(n)); 
        
    }
}

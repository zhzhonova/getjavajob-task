/*
Даны целые числа h, m, s (0 < h ≤ 23, 0 ≤ m ≤ 59, 0 ≤ s ≤ 59), указывающие
момент времени: "h часов, m минут, s секунд". Определить угол (в градусах)
между положением часовой стрелки в начале суток и в указанный момент
времени.
*/
import java.util.Scanner;

public class TaskCh2N39 {
  public static void main(String args[]) {
    
    System.out.println("Введите значения для переменных h, m, s (0 < h ≤ 23, 0 ≤ m ≤ 59, 0 ≤ s ≤ 59)");
    Scanner input = new Scanner(System.in);
    double h = input.nextDouble();
    double m = input.nextDouble();
    double s = input.nextDouble();
    
    if (h>12)  {
    h = h - 12;
    }
    
    double angle = (h*3600+m*60+s)/12/3600*360;

    System.out.println(angle);
  }
}


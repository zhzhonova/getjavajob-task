import java.util.ArrayList;
import java.util.Scanner;
/*
Удалить из массива все повторяющиеся элементы, оставив их первые вхождения, 
т. е. в массиве должны остаться только различные элементы.
*/

public class TaskCh11N158 { 
  
  public static void main(String[] args) {
    
        Scanner input = new Scanner(System.in); 
        System.out.println("Вводите числа массива, начиная с последнего элемента. Чтобы закончить ввод элементов массива, введите ноль");
        ArrayList n = new ArrayList();
        int s = 0;
        Boolean x = true;
        while (x) {
            s = input.nextInt();
            if (s != 0) {
              n.add(0, s);
            }
           else {
             x = false;
           }
        }

    for(int i=0;i < n.size();i++){
        for(int j=i+1;j < n.size();j++){
            if(n.get(i).equals(n.get(j))){
                n.remove(j);
                n.add(n.size(), 0);
                }
        }
    }
        
        String l = "";
        for (int i = 0; i < n.size(); i++) {
        l = l + n.get(i) + " ";
        }

     System.out.println("Ваш очищенный от дубликатов массив: " + l);

}

    
}

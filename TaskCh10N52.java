//Написать рекурсивную процедуру для вывода на экран цифр натурального числа в обратном порядке.
import java.util.Scanner;
public class TaskCh10N52 { 
  
public static int recursion(int n) {
        if (n < 10) {
            return n;
        }
        else {
            System.out.print(n % 10 + " ");
            return recursion(n / 10);
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите любое натуральное число");
        int n = input.nextInt();
        System.out.print(recursion(n));
}
}

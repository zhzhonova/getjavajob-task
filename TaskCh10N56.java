    
/*
Написать рекурсивную функцию, определяющую, является ли заданное на-
туральное число простым (простым называется натуральное число, боль-
шее 1, не имеющее других делителей, кроме единицы и самого себя).
*/

public class TaskCh10N56 { 

private static boolean isPrime(int number, int divisor) {
      if (number == divisor || divisor == 1) 
        return true;
        
      if (number % divisor == 0) 
        return false;
      else
        return isPrime(number, divisor - 1);
    
}

public static void main(String[] args) {
      System.out.println(isPrime(13, 9)); 

}
}

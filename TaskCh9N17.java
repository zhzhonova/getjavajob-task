import java.util.Scanner;
//Дано слово. Верно ли, что оно начинается и оканчивается на одну и ту же букву?


public class TaskCh9N17 {
  public static void main(String args[]) {

          System.out.println("Введите слово");
          Scanner input = new Scanner(System.in);
          String word = input.next();
          char[] strToArray = word.toCharArray(); // Преобразуем строку str в массив символов (char)
          int lastLetter = strToArray.length;

          
          if (strToArray[0]==strToArray[lastLetter-1]) {
            System.out.println("Первая буква слова равна последней");
                } else {
            System.out.println("Слово начинается и оканчивается на разные буквы");

                }

  }
}

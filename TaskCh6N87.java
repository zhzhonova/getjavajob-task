import java.util.Scanner;

    public class TaskCh6N87 {
      public static boolean flag = true;
      public static int score1 = 0;
      public static int score2 = 0;
      public static String team1;
      public static String team2;


      public static void main(String[] args) {
        
      System.out.println("Enter team #1 name: ");
      Scanner input = new Scanner(System.in);
      team1 = input.next();
      
      System.out.println("Enter team #2 name: ");
      team2 = input.next();
      

      while(flag) {
        Scanner input1 = new Scanner(System.in);
        System.out.println("Enter team to score (1 or 2 or 0 (exit)): ");
        int teamToScore = input1.nextInt();
        if (teamToScore == 0) {
         exit();
         } else {

        System.out.println("Enter team score (1 or 2 or 3): ");
        int score = input1.nextInt();
        addScore(score, teamToScore);
        intermediateScore();
         }
      }
      

    }
      static void exit() {
          flag = false;
          if (score1 > score2) {
            System.out.print("Team " + team1 + " is winner with the score: " + score1);
          } else {
            System.out.print("Team " + team2 + " is winner with the score: " + score2);
          }
      }
      
      static void intermediateScore() {
            System.out.println("The score of team " + team1 + " is: " + score1);
            System.out.println("The score of team " + team2 + " is: " + score2);
      }
      
      static void addScore(int s, int team) {
        if (team == 1) {
          score1 += s;
        } else {
          score2 += s;
        }
      }
      

    }

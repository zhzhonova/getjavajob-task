/*
В области 12 районов. Известны количество жителей (в тысячах человек)
и площадь (в км2) каждого района. Определить среднюю плотность населения
по области в целом.
*/

    public class TaskCh5N64 {
      public static void main(String args[]) {
    
    double sum = 0;
    double[] population = {10, 20, 22, 41, 51, 63, 78, 89, 94, 32, 18, 12};
    double[] area = {500, 856, 945, 643, 654, 123, 653, 976, 365, 444, 333, 735};


    for(int i = 0; i < 12; i++) {
      sum = sum + population[i] / area[i];
        if (i == 11) break;
    }
      	System.out.println(sum/12);

  }
}

import java.util.Scanner;
//9.15. Дано слово. Вывести на экран его k-й символ. 


public class TaskCh9N15 {
  public static void main(String args[]) {

    String str = "Волшебство";
    System.out.println("Введите значение k-ого символа, который вы хотите увидеть в слове \"Волшебство\"");
    Scanner input = new Scanner(System.in);
    int k = input.nextInt();
    char chr = str.charAt(k);
    System.out.println("Это буква " + chr);
    
  }
}

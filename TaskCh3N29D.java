//д) только одно из чисел X, Y и Z кратно пяти;

import java.util.Scanner;
import java.util.*;

public class TaskCh3N29D {
  public static void main(String args[]) {
    
    System.out.println("Введите значения для переменных x, y, z");
    Scanner input = new Scanner(System.in);
    Calendar c=Calendar.getInstance(); 

    int curentYear=c.get(c.YEAR); 
    int curentMonth=c.get(c.MONTH)+1;
    int x = input.nextInt();
    int y = input.nextInt();
    int z = input.nextInt();
    
    boolean result = (x%5==0 ^ y%5==0 ^ z%5==0 ^ x%5==0 & y%5==0 & z%5==0);

    System.out.println(result);
  }
}


//а) каждое из чисел X и Y нечетное;

import java.util.Scanner;

public class TaskCh3N29A {
  public static void main(String args[]) {
    
    System.out.println("Введите значения для переменных x, y");
    Scanner input = new Scanner(System.in);
    int x = input.nextInt();
    int y = input.nextInt();

    boolean result = (x%2==0 & y%2==0);

    System.out.println(result);
  }
}


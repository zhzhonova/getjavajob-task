/*
Известны год и номер месяца рождения человека, а также год и номер месяца
сегодняшнего дня (январь — 1 и т. д.). Определить возраст человека (число
полных лет). В случае совпадения указанных номеров месяцев считать, что
прошел полный год.
*/
import java.util.Scanner;

public class TaskCh4N15 {
  public static void main(String args[]) {
    
    System.out.println("Введите ваши год и номер месяца вашего рождения");
    Scanner input = new Scanner(System.in);
    int year = input.nextInt();
    int month = input.nextInt();
    
    int currentYear = 2017;
    int currentMonth = 11;
    int age = 0;

    if (month <= currentMonth) {
      age = currentYear - year;
    }
    else {
      age = currentYear - year - 1;
    }
    
    System.out.println(age);

  }
}


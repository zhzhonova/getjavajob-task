import java.util.Scanner;


/*В трехзначном числе x зачеркнули его вторую цифру. Когда к образованному
при этом двузначному числу справа приписали вторую цифру числа x, то по-
лучилось число n. По заданному n найти число x (значение n вводится с кла-
виатуры, 100 ≤ n ≤ 999).
*/

public class TaskCh2N31 {
  public static void main(String args[]) {

    System.out.println("Введите трехзначное число n промежутке 100 ≤ n ≤ 999");
    Scanner input = new Scanner(System.in);
    int n = input.nextInt();
    
    int x3 = n / 10 % 10; // третья цифра числа x
    int x2 = n % 10; // вторая цифра
    int x1 = n / 100 % 10; // первая цифра
    int x = x1 * 100 + x2 * 10 + x3;

    System.out.println("x = " + x);

    
  }
}

//Написать рекурсивную функцию для вычисления максимального элемента массива из n элементов.
public class TaskCh10N48 { 
  

public static int findMax(int[] n, int max) {
    if (n.length > max) {
        int next = findMax(n, max + 1);
        return (n[max] > next) ? n[max] : next;
    } else {
        return n[0];
    }
}

    public static void main(String[] args) {
      
    int[] l = {2, 3, 174, 5, 0, 345};
    System.out.println("Максимальный элемент массива: " + findMax(l, 0));
}
}

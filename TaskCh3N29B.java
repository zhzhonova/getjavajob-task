//б) только одно из чисел X и Y меньше или равно 2;

import java.util.Scanner;

public class TaskCh3N29B {
  public static void main(String args[]) {
    
    System.out.println("Введите значения для переменных x, y");
    Scanner input = new Scanner(System.in);
    int x = input.nextInt();
    int y = input.nextInt();
    
    boolean result = (x<=2)^(y<=2);

    System.out.println(result);
  }
}

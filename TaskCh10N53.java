    
/*
Написать рекурсивную процедуру для ввода с клавиатуры последовательности 
чисел и вывода ее на экран в обратном порядке (окончание последовательности — при вводе нуля).
*/

public class TaskCh10N53 { 
    public static void recursion(int[] n, int i) {
      
    if (n.length > i) {
        recursion(n, i + 1);
        System.out.println(n[i]);
    }
}
   
public static void main(String[] args) {
      int[] n = {1, 45, 7, 5, 89, 45, 34};
      recursion(n, 0); 
    }
}

    
//Даны первый член и разность арифметической прогрессии. Написать рекурсивную функцию для нахождения:
//а) n-го члена прогрессии;
//б) суммы n первых членов прогрессии.

import java.util.Scanner;
    
    public class TaskCh10N45 { 
        public static int prog(int x, int y, int z) {
          
            if (z == 1) {
                return x;
            }
              return prog(x, y, z - 1) + y;
        }


    public static int sum(int x, int y, int z) {
      
        if (z == 0) {
            return 0;
        }
              return sum(x, y, z - 1) + x + (z - 1) * y;
    }

    
    public static void main(String[] args) {
      
        Scanner input = new Scanner(System.in);
        System.out.println("Дана арифметическая прогрессия. Введите ее первый член a1, разность d, а также n номер члена для нахождения его значения и суммы n первых членов.");
        int a1 = input.nextInt();
        int d = input.nextInt();
        int n = input.nextInt();
        
        System.out.println("Значение n-го члена прогрессии: " + prog(a1, d, n)); 
        System.out.println("Cуммы n первых членов прогрессии: " + sum(a1, d, n)); 
    }
}

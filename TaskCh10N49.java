//Написать рекурсивную функцию для вычисления индекса максимального элемента массива из n элементов.

public class TaskCh10N49 { 
  
public static int findMax(int[] n, int max) {
    if (n.length > max) {
        int next = findMax(n, max + 1);
        return (max > next) ? max : next;
    } else {
        return 0;
    }
}

    public static void main(String[] args) {
      
    int[] l = {2, 3, 174, 5, 0, 345};
    System.out.println("Максимальный элемент массива: " + findMax(l, 0));
}
}

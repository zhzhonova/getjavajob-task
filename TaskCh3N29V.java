//в) хотя бы одно из чисел X и Y равно нулю;

import java.util.Scanner;

public class TaskCh3N29V {
  public static void main(String args[]) {
    
    System.out.println("Введите значения для переменных x, y");
    Scanner input = new Scanner(System.in);
    int x = input.nextInt();
    int y = input.nextInt();
    
    boolean result = (x==0 | y==0);

    System.out.println(result);
  }
}


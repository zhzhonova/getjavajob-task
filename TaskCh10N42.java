    
//Написать рекурсивную функцию для расчета степени n вещественного числа a (n — натуральное число).
import java.util.Scanner;

public class TaskCh10N42 { 
    public static double recursion(double x, int y) {
      
        if (y == 0) {
            return 1;
        }
        return recursion (x, y - 1) * x;
    }
    public static void main(String[] args) {
      
        Scanner input = new Scanner(System.in);
        System.out.println("Введите вещественное число а, которое Вы хотите возвести в степень.");
        double a = input.nextDouble();
        System.out.println("Введите степень n (n - натуральное число).");
        int n = input.nextInt();

        System.out.println(recursion(a, n)); 
        
    }
}

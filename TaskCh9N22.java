import java.util.Scanner;
//Дано слово, состоящее из четного числа букв. Вывести на экран его первую половину, не используя оператор цикла.


public class TaskCh9N22 {
  public static void main(String args[]) {

          System.out.println("Введите слово, состоящее из четного числа букв");
          Scanner input = new Scanner(System.in);
          String word = input.next();
          int wordMiddle = word.length() / 2;
          
          String subStr = word.substring(0, wordMiddle);
          System.out.println(subStr);
          
  }
}

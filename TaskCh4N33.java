import java.util.Scanner;


/*Дано натуральное число.
а) Верно ли, что оно заканчивается четной цифрой?
б) Верно ли, что оно заканчивается нечетной цифрой?
*/

public class TaskCh4N33 {
  public static void main(String args[]) {

    System.out.println("Введите любое натуральное число");
    Scanner input = new Scanner(System.in);
    int x = input.nextInt();
    int x1 = x % 10; //смотрим последнюю цифру

    if (x1 % 2 == 0) {
    System.out.println("Число заканчивается четной цифрой");
    }
    else {
    System.out.println("Число заканчивается нечетной цифрой");
    }
    
  }
}

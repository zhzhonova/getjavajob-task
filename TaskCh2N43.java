//Даны два целых числа a и b. Если a делится на b или b делится на a, то вывес-ти 1, иначе — любое другое число. Условные операторы и операторы цикла не использовать.
import java.util.Scanner;

public class TaskCh2N43 {
  public static void main(String[] args) {
    
    System.out.println("Введите числа а и b");
    Scanner input = new Scanner(System.in);
    int a = input.nextInt();
    int b = input.nextInt();
    System.out.println((a%b)*(b%a)+1);
 }
}


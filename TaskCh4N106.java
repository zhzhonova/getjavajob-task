import java.util.Scanner;


/*Составить программу, которая в зависимости от порядкового номера дня ме-
сяца (1, 2, ..., 12) выводит на экран время года, к которому относится этот
месяц.
*/

public class TaskCh4N106 {
  public static void main(String args[]) {

    System.out.println("Введите число месяца m");
    Scanner input = new Scanner(System.in);
    int m = input.nextInt();
    
    
    switch(m) {
      case 1:
        System.out.println("январь");
        break;
      case 2:
        System.out.println("февраль");
        break;
      case 3:
        System.out.println("март");
        break;
      case 4:
        System.out.println("апрель");
        break;
      case 5:
        System.out.println("май");
        break;
      case 6:
        System.out.println("июнь");
        break;
      case 7:
        System.out.println("июль");
        break;
      case 8:
        System.out.println("август");
        break;
      case 9:
        System.out.println("сентябрь");
        break;
      case 10:
        System.out.println("октябрь");
        break;
      case 11:
        System.out.println("ноябрь");
        break;
      case 12:
        System.out.println("декабрь");
        break;
    }
  }
}

import java.util.ArrayList;
import java.util.List;

public class TaskCh13N12 {
    public static void main(String[] args) {
        /*
        Известна информация о 20 сотрудниках фирмы: фамилия, имя, отчество,
адрес и дата поступления на работу (месяц, год). Напечатать фамилию,
имя, отчество и адрес сотрудников, которые на сегодняшний день прорабо-
тали в фирме не менее трех лет. День месяца не учитывать (при совпадении
месяца поступления и месяца сегодняшнего дня считать, что прошел пол-
ный год).
         */
        List<Employee> employees = new ArrayList<Employee>();
        employees.add(new Employee("Petr", "Petrovich", "Ivanov", "Small Street", 1, 2017));
        employees.add(new Employee("Ivan", "Petrov", "Petrovich", "Small Street", 2, 2017));
        employees.add(new Employee("Oleg", "Petrov", "Petrovich", "Small Street", 3, 2016));
        employees.add(new Employee("Maxim", "Petrov", "Petrovich", "Small Street", 4, 2016));
        employees.add(new Employee("Aleksander", "Petrov", "Petrovich", "Small Street", 5, 2013));
        employees.add(new Employee("Stepan", "Petrov", "Petrovich", "Small Street", 6, 2016));
        employees.add(new Employee("Vasilii", "Petrov", "Sidorov", "Small Street", 7, 2016));
        employees.add(new Employee("Igor", "Petrov", "Petrovich", "Small Street", 8, 2016));
        employees.add(new Employee("Lev", "Petrov", "Petrovich", "Small Street", 9, 2003));
        employees.add(new Employee("Michiel", "Petrov", "Petrovich", "Small Street", 12, 2014));
        employees.add(new Employee("Alexey", "Petrov", "Petrovich", "Small Street", 10, 2016));
        employees.add(new Employee("Petr", "Ivanovich", "Petrovich", "Small Street", 5, 2011));
        employees.add(new Employee("Petr", "Prokofievich", "Petrovich", "Small Street", 5, 2016));
        employees.add(new Employee("Petr", "Petrov", "Petrovich", "Small Street", 5, 2005));
        employees.add(new Employee("Sergey", "Petrov", "Petrovich", "Small Street", 5, 2006));
        employees.add(new Employee("Petr", "Petrov", "Petrovich", "Small Street", 5, 2013));
        employees.add(new Employee("Daniil", "Petrov", "Petrovich", "Small Street", 11, 2010));
        employees.add(new Employee("Petr", "Petrov", "Petrovich", "Small Street", 12, 2001));
        employees.add(new Employee("Petr", "Petrov", "Petrovich", "Small Street", 4, 2011));
        employees.add(new Employee("Petr", "Petrov", "Petrovich", "Small Street", 5, 2016));

        for (Employee emp : employees) {
            if (emp.startYear <= 2014) System.out.println(emp);
        }
    }
}

public class Employee {

    public String name;
    public String secondName;
    public String middleName;
    public String address;
    public int startMonth;
    public int startYear;

    public Employee(String name, String secondName, String middleName, String address, int startMonth, int startYear) {
        super();
        this.name = name;
        this.secondName = secondName;
        this.middleName = middleName;
        this.address = address;
        this.startMonth = startMonth;
        this.startYear = startYear;


    }
    @Override
    public String toString() {
        return "Employee [name=" + name + ", secondName=" + secondName + ", middleName=" + middleName
                + ", address=" + address + ", startMonth=" + startMonth + ", startYear=" + startYear + "]";
    }
}

//Написать рекурсивную функцию для вычисления факториала натурального числа n.
import java.util.Scanner;

public class TaskCh10N41 { 
    public static int recursion(int n) {
      
        if (n == 1) {
            return 1;
        }
        return recursion(n - 1) * n;
    }
    public static void main(String[] args) {
      
        Scanner input = new Scanner(System.in);
        System.out.println("Введите факториал числа, которое вы хотите вычислить");
        int factorial = input.nextInt();
    
        System.out.println(recursion(factorial)); 
        
    }
}

import java.util.Scanner;
import java.lang.StringBuilder;

//Составить программу, которая печатает заданное слово, начиная с последней буквы.

public class TaskCh9N42 {
  public static void main(String args[]) {
    
          System.out.println("Введите слово");
          Scanner input = new Scanner(System.in);
          String word = input.next();
          
          StringBuilder builder = new StringBuilder(word);   
          builder.reverse();
          for (int i = 0; i < builder.length(); i++) {
            System.out.println(builder.charAt(i));
        }
          
  }
}

import java.util.Scanner;
import java.lang.Math;

public class TaskCh1N17 {
  public static void main(String args[]) {
    
    System.out.println("Введите значения для переменных x, a, b, c");
    Scanner input = new Scanner(System.in);
    double x = input.nextDouble();
    double a = input.nextDouble();
    double b = input.nextDouble();
    double c = input.nextDouble();
    
    double o = Math.sqrt(1 - Math.pow(Math.sin(x),2));
    double p = (Math.sqrt(x+1) + Math.sqrt(x-1))/ 2 * Math.sqrt(x);
    double r = Math.abs(x) + Math.abs(x+1);
    double s = Math.abs(1 - Math.abs(x));

    System.out.println(o);
    System.out.println(p);
    System.out.println(r);
    System.out.println(s);
  }
}


import java.util.Scanner;


/*Дано трехзначное число. Найти число, полученное при прочтении его цифр
справа налево. 200>n>100 
*/

public class TaskCh2N13 {
  public static void main(String args[]) {

    System.out.println("Введите трехзначное число в промежутке 200>n>100");
    Scanner input = new Scanner(System.in);
    int num = input.nextInt();
    while (num != 0) {
    System.out.print(num % 10); // Печатаем крайнюю правую цифру числа `num`
    num = num / 10; // Изменяем переменную `num` таким образом, чтобы она стала равна числу из оставшихся цифр
    }
    
  }
}


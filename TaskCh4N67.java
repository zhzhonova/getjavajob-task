import java.util.Scanner;


/*Дано целое число k (1 >= k <= 365). Определить, каким будет k-й день года: вы-
ходным (суббота и воскресенье) или рабочим, если 1 января — понедельник.
*/

public class TaskCh4N67 {
  public static void main(String args[]) {

    System.out.println("Введите целое число k");
    Scanner input = new Scanner(System.in);
    int k = input.nextInt();
    double x = k % 7;
    
    if (x > 0 & x < 6) {
      System.out.println("Сегодня рабочий день. Трудись бедняга!");
    }
    else {
      System.out.println("Сегодня выходной. Но не забывай учить Java!");
    }

  }
}

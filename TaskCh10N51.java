//Написать рекурсивную процедуру для вывода на экран цифр натурального числа в обратном порядке.
public class TaskCh10N51 { 
  
public static void recursion1(int n) {
        if (n > 0) {
            System.out.println(n);
            recursion1(n - 1);
        }
    }
    
public static void recursion2(int n) {
        if (n > 0) {
            recursion2(n - 1);
            System.out.println(n);
        }
    }
    
public static void recursion3(int n) {
        if (n > 0) {
            recursion3(n - 1);
            System.out.println(n);
        }
            System.out.println(n);
    }

    public static void main(String[] args) {
        int n = 5;
        System.out.println("Результат исполнения первой процедуры: ");
        recursion1(n);
        System.out.println("Результат исполнения второй процедуры: ");
        recursion2(n);
        System.out.println("Результат исполнения третьей процедуры: ");
        recursion3(n);
}
}

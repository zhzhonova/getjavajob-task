//Написать рекурсивную функцию для вычисления k-го члена последовательности Фибоначчи.
import java.util.Scanner;
    
    public class TaskCh10N47 { 
        public static int fib(int i) {
            if (i == 1) return 1;
            if (i == 2) return 1;
            return fib(i - 1) + fib(i - 2);        
          
        }


    public static void main(String[] args) {
      
        Scanner input = new Scanner(System.in);
        System.out.println("Дана последовательность Фибоначчи. Введите член k, который Вы хотите вычислить.");
        int k = input.nextInt();

        System.out.println("Значение k-го члена последовательности Фибоначчи: " + fib(k)); 
    }
}

import java.util.Scanner;

//Напечатать таблицу перевода 1, 2, ... 20 долларов США в рубли по текущему курсу (значение курса вводится с клавиатуры).


public class TaskCh5N10 {
  public static void main(String args[]) {

    Scanner input = new Scanner(System.in);
    System.out.println("Введите значение текущего курса");
    double rate = input.nextDouble();
    
    int[] value = new int[21];
    
    for (int i = 0; i < value.length; i++) {
	  value[i] = i;
	  System.out.println("Значение в рублях для: " + value[i] + " долл. " + " = " + value[i]*rate + " руб. ");
	  
    }
  }
}	  



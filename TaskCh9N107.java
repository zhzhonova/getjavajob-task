import java.util.Scanner;
import java.lang.Math;
//Дано слово. Поменять местами первую из букв а и последнюю из букв о. Учесть возможность того, что таких букв в слове может не быть.

public class TaskCh9N107 {
  public static void main(String args[]) {
    
          System.out.println("Enter a word. Use Latyn symbols only!!!");
          Scanner input = new Scanner(System.in);
          String word = input.next();
          
          int lastO = word.lastIndexOf('o');
          int firstA = word.indexOf('a');
          
          String word2 = word.replaceFirst("a", "o");
          
          char[] word3 = word2.toCharArray(); 

          for (int i=0; i < word3.length; i++) {
            if (lastO != -1) {
              word3[lastO] = 'a';
              }
          }
          System.out.println(word3);

  }
}

/*
Дано натуральное число. Определить, какая цифра встречается в нем чаще:
0 или 9
*/
import java.util.Scanner;
import java.lang.Math;

    public class TaskCh6N8 {
      public static void main(String args[]) {
        
      int numOfnine = 0;
      int numOfzero = 0;
            
      Scanner input = new Scanner(System.in); 
      System.out.println("Введите длину числа");
      int size = input.nextInt(); 
      int array[] = new int[size]; 
      System.out.println("Введите любое натуральное число заданной длины, каждую цифру вводите через пробел");
      
      for (int i = 0; i < size; i++) {
        array[i] = input.nextInt(); 
        int x = array[i];
        if (Math.sqrt(x) == 3) numOfnine += 1;
        else if (Math.sqrt(x) == 0) numOfzero += 1;
      }
      
      if (numOfzero > numOfnine) {
        System.out.println("Цифра девять встречается чаще в вашем числе, чем цифра ноль!");
      } else if (numOfzero < numOfnine) {
        System.out.println("Цифра девять встречается чаще в вашем числе, чем цифра ноль!");
      } else {
        System.out.println("Цифра девять и цифра ноль встречаются в вашем числе одинаково часто!");
      }
      
}
}
